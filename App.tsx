import React from 'react';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import HomeScreen from "./src/screens/HomeScreen";

export const customTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: 'tomato',
        accent: 'yellow',
    },
};

export default function App() {
    return (
        <PaperProvider theme={customTheme}>
            <HomeScreen/>
        </PaperProvider>
    );
}
