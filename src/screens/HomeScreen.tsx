import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import { Text } from "react-native-paper";
import { customTheme } from "../../App";

const HomeScreen = () => {
    return (
        <SafeAreaView style={styles.container}>
            <Text>Home screen</Text>
        </SafeAreaView>
    );
}

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: customTheme.colors.primary
    }
});
